package com.example.studicase3;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {
    Dialog dialog;
    private RecyclerView mRecyclerView;
    private ArrayList<User> daftarUser;
    private AdapterUser adapterUntukUser;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mRecyclerView = findViewById(R.id.idRecyclerView);

        int gridColumnCount =getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,gridColumnCount));


        daftarUser = new ArrayList<>();

        if (savedInstanceState!=null){

            daftarUser.clear();
            for (int i = 0; i <savedInstanceState.getStringArrayList("nama").size() ; i++) {
                daftarUser.add(new User(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("pekerjaan").get(i),
                        savedInstanceState.getIntegerArrayList("gender").get(i)));
            }
        }else {

            init();
        }

        adapterUntukUser = new AdapterUser(daftarUser,this);
        mRecyclerView.setAdapter(adapterUntukUser);

        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();
                Collections.swap(daftarUser, from ,to);
                adapterUntukUser.notifyItemMoved(from,to);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                daftarUser.remove(viewHolder.getAdapterPosition());
                adapterUntukUser.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        helper.attachToRecyclerView(mRecyclerView);

    }

    void init(){
        daftarUser.clear();
        daftarUser.add(new User("Jonny","Android Dev",1));
        daftarUser.add(new User("Jessica","Front End",2));

    }


    void tambah(View view){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.add);
        final TextView mNama,mPekerjaan;
        final Spinner mGender;
        mNama = dialog.findViewById(R.id.edName);
        mPekerjaan = dialog.findViewById(R.id.edPekerjaan);

        TextView tambah=dialog.findViewById(R.id.txTambahUser);
        TextView batal = dialog.findViewById(R.id.txBatal);

        mGender = dialog.findViewById(R.id.spGender);

        String[]list={"Male","Female"};

        ArrayAdapter<String>adapterX = new ArrayAdapter(dialog.getContext(),android.R.layout.simple_spinner_item,list);
        mGender.setAdapter(adapterX);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftarUser.add(new User(mNama.getText().toString(),mPekerjaan.getText().toString(),mGender.getSelectedItemPosition()+1));
                adapterUntukUser.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ArrayList<String>tempListNama = new ArrayList<>();
        ArrayList<String>tempListPekerjaan = new ArrayList<>();
        ArrayList<Integer>tempListGender = new ArrayList<>();
        for (int i = 0; i <daftarUser.size() ; i++) {
            tempListNama.add(daftarUser.get(i).getNama());
            tempListPekerjaan.add(daftarUser.get(i).getPekerjaan());
            tempListGender.add(daftarUser.get(i).getAvatar());
        }
        outState.putStringArrayList("nama",tempListNama);
        outState.putStringArrayList("pekerjaan",tempListPekerjaan);
        outState.putIntegerArrayList("gender",tempListGender);
        super.onSaveInstanceState(outState);

    }
}